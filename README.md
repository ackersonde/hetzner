[![pipeline status](https://gitlab.com/ackersonde/hetzner/badges/main/pipeline.svg)](https://gitlab.com/ackersonde/hetzner/-/commits/main)

# Hetzner

Since DigitalOcean has increased their prices and Hetzner has some 4x the offerings (half the price + double the resources) I've moved my [homepage](https://ackerson.de), [slack](https://gitlab.com/ackersonde/bender-slackbot) & [telegram](https://gitlab.com/ackersonde/telegram-bot) bots over.

# Build & Deploy [Hetzner](https://console.hetzner.cloud/projects/1200165/servers)

Using the Hetzner's golang api from [godo](https://pkg.go.dev/github.com/hetznercloud/hcloud-go), every push to this repository creates a [custom](https://gitlab.com/ackersonde/hetzner/-/blob/main/scripts/raw_debian_userdata.sh) Debian <img src="https://upload.wikimedia.org/wikipedia/commons/thumb/4/4a/Debian-OpenLogo.svg/182px-Debian-OpenLogo.svg.png" width="16"> instance in Nuremberg.

# Automated Deployment (DEPRECATED)

I have a [monthly cronjob](https://gitlab.com/ackersonde/pi-ops/-/blob/master/scripts/crontab_cakefor.txt) running on one of my raspberry PIs which triggers this deployment after regenerating the SSL certificate ([only valid for 10d](https://gitlab.com/ackersonde/pi-ops/-/blob/master/scripts/gen_new_deploy_keys.sh#L26)) required for login.

# Upgrade Traefik

You should regularly upgrade Traefik by logging into the server, stopping the compose:

```
homepage:~# docker compose -f ./traefik/docker-compose-traefik.yaml down
homepage:~# vi ./traefik/docker-compose-traefik.yaml down
...
    image: traefik:<new.version.tag>
...
homepage:~# docker compose -f ./traefik/docker-compose-traefik.yaml pull
homepage:~# docker compose -f ./traefik/docker-compose-traefik.yaml up -d
```

# WARNING

Every push to this repo will result in a new server created at Hetzner => +$4 / month, tearing down and redeploying websites and bots while also updating DNS entries for \*.ackerson.de.

Use git commit msg string snippet `[skip ci]` to avoid this.
