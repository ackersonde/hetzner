package main

import (
	"context"
	"crypto"
	"crypto/ed25519"
	"crypto/rand"
	"encoding/base64"
	"encoding/pem"
	"flag"
	"fmt"
	"log"
	"net"
	"net/netip"
	"os"
	"strconv"
	"strings"
	"time"

	"github.com/hetznercloud/hcloud-go/hcloud"
	"gitlab.com/ackersonde/hetzner/hetznercloud"
	"golang.org/x/crypto/ssh"
)

var sshPrivateKeyFilePath = "/tmp/id_ed25519"
var envFile = "/tmp/new_hetzner_server_params"
var HETZNER_DNS_API_TOKEN = os.Getenv("HETZNER_DNS_API_TOKEN")
var HETZNER_ACKERSONDE_ZONEID = os.Getenv("HETZNER_ACKERSONDE_ZONEID")
var instanceTag = "docker"

func main() {
	client := hcloud.NewClient(hcloud.WithToken(hetznercloud.HETZNER_API_TOKEN))

	fnPtr := flag.String("fn", "createServer|cleanupDeploy|firewallSSH|createSnapshot|checkServer", "which function to run")
	ipPtr := flag.String("ip", "<internet ip addr of gitlab action instance>", "see prev param")
	serverPtr := flag.Int("serverID", 0, "server ID to check")
	flag.Parse()

	if *fnPtr == "createServer" {
		createServer(client)
	} else if *fnPtr == "cleanupDeploy" {
		cleanupDeploy(client, *serverPtr)
	} else if *fnPtr == "firewallSSH" {
		allowSSHipAddress(client, *ipPtr)
	} else if *fnPtr == "checkServer" {
		checkServerPowerSwitch(client, *serverPtr)
	} else {
		log.Printf("Sorry, I don't know `%s`. Check valid params with `./hetzner --help`", *fnPtr)
	}

	/* For checking out new server & image types:
	types, _ := client.ServerType.All(context.Background())
	for _, typee := range types {
		fmt.Printf("type[%d] %s x %d cores (%f RAM)\n", typee.ID,
			typee.Description, typee.Cores, typee.Memory)
	}

	images, _ := client.Image.All(context.Background())
	for _, image := range images {
		fmt.Printf("image[%d] %s (%s)\n", image.ID, image.Name, image.Architecture)
	}

	existingServer := getExistingServer(client, instanceTag)
	fmt.Printf("Running https://console.hetzner.cloud/projects/2279047/servers/%d/overview @ %s\n", existingServer.ID, existingServer.PublicNet.IPv6.IP.String())
	*/
}

func allowSSHipAddress(client *hcloud.Client, ipAddr string) {
	ctx := context.Background()

	ipAddr = strings.Trim(ipAddr, "\"") // remove quotes from string
	ip, _ := netip.ParseAddr(ipAddr)
	mask := net.CIDRMask(32, 32)
	if ip.Is6() {
		mask = net.CIDRMask(128, 128)
	}

	opts := hcloud.FirewallCreateOpts{
		Name:   "gitlabBuildDeploy-" + os.Getenv("CI_PIPELINE_ID"),
		Labels: map[string]string{"access": "gitlab"},
		Rules: []hcloud.FirewallRule{{
			Direction: hcloud.FirewallRuleDirectionIn,
			SourceIPs: []net.IPNet{{
				IP:   net.ParseIP(ipAddr),
				Mask: mask,
			}},
			Protocol: "tcp",
			Port:     String("22"),
		}},
		ApplyTo: []hcloud.FirewallResource{{
			Type: hcloud.FirewallResourceTypeLabelSelector,
			LabelSelector: &hcloud.FirewallResourceLabelSelector{
				Selector: "label=" + instanceTag},
		}},
	}
	result, response, err := client.Firewall.Create(ctx, opts)
	if err != nil {
		log.Printf("NOPE: %s (%s)", opts.Name, err.Error())
		if strings.Contains(err.Error(), "uniqueness_error") {
			removeDeploymentFirewalls(client, ctx, instanceTag, "access=gitlab")
			allowSSHipAddress(client, ipAddr) // retry
		}
	} else {
		log.Printf("%s: %s", response.Status, result.Firewall.Name)
	}
}

func checkServerPowerSwitch(client *hcloud.Client, serverID int) {
	ctx := context.Background()
	if serverID != 0 {
		server, _, _ := client.Server.GetByID(ctx, serverID)
		if server.Status != hcloud.ServerStatusRunning {
			client.Server.Poweron(ctx, server)
		}
	}
}

func createServer(client *hcloud.Client) {
	ctx := context.Background()

	// find existing server
	existingServer := getExistingServer(client, instanceTag)

	// prepare new server
	timestamp := strconv.FormatInt(time.Now().Unix(), 10)

	deploymentKey := createSSHKey(client, "id"+os.Getenv("CI_PIPELINE_ID")+"_"+timestamp)

	debianUserData, _ := os.ReadFile("debian_userdata.sh")

	serverOpts := hcloud.ServerCreateOpts{
		Name:       instanceTag + "-id" + os.Getenv("CI_PIPELINE_ID") + "-" + timestamp + ".ackerson.de",
		ServerType: &hcloud.ServerType{ID: 93},     // cax21(id:93)=4 core, 8GB --OR-- cax11(id:45)=Arm64 2 core, 4GB Ram
		Image:      &hcloud.Image{ID: 114690389},   // 103908130: ubuntu-22.04 (arm) , 114690389: Debian 12 (arm)
		Location:   &hcloud.Location{Name: "nbg1"}, // nbg1=Nuremberg , fsn1=Falkenstein
		Labels:     map[string]string{"label": instanceTag},
		Automount:  Bool(false),
		UserData:   string(debianUserData),
		SSHKeys:    []*hcloud.SSHKey{{ID: 10839156}, {ID: 19756520}, deploymentKey}, // default ssh key in hetzner
	}

	result, _, err := client.Server.Create(ctx, serverOpts)

	if err != nil {
		log.Fatalf("*** unable to create server: %s\n", err)
	}

	if result.Server == nil {
		log.Fatalf("*** no server created?\n")
	} else {
		client.Server.EnableBackup(ctx, result.Server, "")
		existingServerVars := ""
		if existingServer.Name != "" {
			existingServerVars = "\nexport OLD_SERVER_IPV6=" +
				existingServer.PublicNet.IPv6.IP.String() + "1\nexport OLD_SERVER_ID=" + strconv.Itoa(existingServer.ID)

			// update existingServer Label with "delete":"true" !
			client.Server.Update(ctx, existingServer, hcloud.ServerUpdateOpts{
				Labels: map[string]string{"delete": "true"},
			})
		}

		// Write key metadata from existing/new servers
		envVarsFile := []byte(
			"export NEW_SERVER_IPV4=" + result.Server.PublicNet.IPv4.IP.String() +
				"\nexport NEW_SERVER_IPV6=" + result.Server.PublicNet.IPv6.IP.String() + "1" +
				"\nexport NEW_SERVER_ID=" + strconv.Itoa(result.Server.ID) +
				existingServerVars)

		err = os.WriteFile(envFile, envVarsFile, 0644)
		if err != nil {
			log.Fatalf("Failed to write %s: %s\n", envFile, err)
		} else {
			log.Printf("wrote %s\n", envFile)
		}
	}
}

func cleanupDeploy(client *hcloud.Client, serverID int) {
	ctx := context.Background()

	deployKeys, _ := client.SSHKey.AllWithOpts(ctx, hcloud.SSHKeyListOpts{
		ListOpts: hcloud.ListOpts{LabelSelector: "access=gitlab"},
	})
	for _, deployKey := range deployKeys {
		if strings.HasPrefix(deployKey.Name, "id"+os.Getenv("CI_PIPELINE_ID")) || deployKey.Created.Before(time.Now().Add(-10*time.Minute)) {
			_, err := client.SSHKey.Delete(ctx, deployKey)
			if err == nil {
				log.Printf("DELETED SSH key %s\n", deployKey.Name)
			} else {
				log.Fatalf("Unable to delete SSH key %s (%s) !!!\n", deployKey.Name, err)
			}
		}
	}

	removeDeploymentFirewalls(client, ctx, instanceTag, "access=gitlab")

	server, _, _ := client.Server.GetByID(ctx, serverID)
	if server != nil {
		// ackerson.de
		res := hetznercloud.UpdateDNSentry(
			server.PublicNet.IPv6.IP.String()+"1", "60",
			"AAAA", "@", "db55783d0ab2e5e19506f9afd57199d8",
			HETZNER_ACKERSONDE_ZONEID, HETZNER_DNS_API_TOKEN)
		if res != "" {
			log.Printf("updating AAAA record for ackerson.de failed: %s", res)
		}
		res = hetznercloud.UpdateDNSentry(
			server.PublicNet.IPv4.IP.String(), "60",
			"A", "@", "bb321175a039962fb595be9a8727c197",
			HETZNER_ACKERSONDE_ZONEID, HETZNER_DNS_API_TOKEN)
		if res != "" {
			log.Printf("updating A record for ackerson.de failed: %s", res)
		}
		// ipv4.ackerson.de
		res = hetznercloud.UpdateDNSentry(
			server.PublicNet.IPv4.IP.String(), "60",
			"A", "ipv4", "0cdef5068d792c4f032da705707f7842",
			HETZNER_ACKERSONDE_ZONEID, HETZNER_DNS_API_TOKEN)
		if res != "" {
			log.Printf("updating A record for ipv4.ackerson.de failed: %s", res)
		}
		// ipv6.ackerson.de
		res = hetznercloud.UpdateDNSentry(
			server.PublicNet.IPv6.IP.String()+"1", "60",
			"AAAA", "ipv6", "4e840f1bbe1e1d3745b95fb7cf19f709",
			HETZNER_ACKERSONDE_ZONEID, HETZNER_DNS_API_TOKEN)
		if res != "" {
			log.Printf("updating AAAA record for ipv6.ackerson.de failed: %s", res)
		}
	} else {
		log.Printf("Unable to find server %d", serverID)
	}

	opts := hcloud.ServerListOpts{ListOpts: hcloud.ListOpts{LabelSelector: "delete=true"}}
	servers, _ := client.Server.AllWithOpts(ctx, opts)
	for _, server := range servers {
		if serverID != 0 { // don't delete unless specifically instructed
			_, _, err := client.Server.DeleteWithResult(ctx, server)
			if err == nil {
				log.Printf("DELETING Server %s\n", server.Name)
			} else {
				log.Fatalf("Unable to delete server %s (%s)!!!\n", server.Name, err)
			}
		}
	}
}

func removeDeploymentFirewalls(client *hcloud.Client, ctx context.Context, instanceTag string, firewallTag string) {
	firewalls, _ := client.Firewall.AllWithOpts(context.Background(), hcloud.FirewallListOpts{
		ListOpts: hcloud.ListOpts{LabelSelector: firewallTag},
	})
	resources := []hcloud.FirewallResource{
		{
			Type: hcloud.FirewallResourceTypeLabelSelector,
			LabelSelector: &hcloud.FirewallResourceLabelSelector{
				Selector: "label=" + instanceTag},
		},
	}
	for _, firewall := range firewalls {
		// on weekly redeploys, we have 4 concurrent builds each with their own deployment key
		// delete the one you created or any > 10mins old
		// TODO: daily security check for unnecessary firewalls + cleanup (e.g. key >1hr old? nuke it...)
		if strings.HasSuffix(firewall.Name, os.Getenv("CI_PIPELINE_ID")) || firewall.Created.Before(time.Now().Add(-10*time.Minute)) {
			actions, _, _ := client.Firewall.RemoveResources(ctx, firewall, resources)
			for _, action := range actions {
				client.Action.WatchProgress(ctx, action)
			}

			for {
				_, err := client.Firewall.Delete(ctx, firewall)
				if err == nil {
					log.Printf("DELETED firewall %s\n", firewall.Name)
					break
				} else {
					log.Printf("waiting for firewall to be released: %s", err)
					time.Sleep(3 * time.Second)
				}
			}
		}
	}
}

func getExistingServer(client *hcloud.Client, tag string) *hcloud.Server {
	ctx := context.Background()
	opts := hcloud.ServerListOpts{ListOpts: hcloud.ListOpts{LabelSelector: "label=" + tag}}
	existingServers, _ := client.Server.AllWithOpts(ctx, opts)
	server := new(hcloud.Server)
	if len(existingServers) == 1 {
		server = existingServers[0]
	}

	return server
}

func Bool(b bool) *bool { return &b }

func String(s string) *string { return &s }

func createSSHKey(client *hcloud.Client, gitlabBuild string) *hcloud.SSHKey {
	pub, priv, err := ed25519.GenerateKey(rand.Reader)
	if err != nil {
		panic(err)
	}
	p, err := ssh.MarshalPrivateKey(crypto.PrivateKey(priv), "")
	if err != nil {
		panic(err)
	}
	privateKeyPem := pem.EncodeToMemory(p)
	err = os.WriteFile(sshPrivateKeyFilePath, privateKeyPem, 0600)
	if err != nil {
		log.Printf("Failed to write private key: %v", err)
		os.Exit(1)
	}

	publicKey, err := ssh.NewPublicKey(pub)
	if err != nil {
		log.Printf("ssh.NewPublicKey returned error: %v", err)
		os.Exit(1)
	}
	publicKeyString := "ssh-ed25519" + " " + base64.StdEncoding.EncodeToString(publicKey.Marshal())
	fmt.Printf("Public Key:\n%s\n", publicKeyString)

	createRequest := hcloud.SSHKeyCreateOpts{
		Name:      gitlabBuild + "SSHkey",
		PublicKey: publicKeyString,
		Labels:    map[string]string{"access": "gitlab"},
	}

	key, _, err := client.SSHKey.Create(context.Background(), createRequest)
	if err != nil {
		log.Printf("client.SSHKey.Create returned error: %v", err)
		os.Exit(1)
	}

	return key
}
