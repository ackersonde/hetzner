package hetznercloud

import (
	"bytes"
	"context"
	"fmt"
	"net/http"
	"os"

	"github.com/hetznercloud/hcloud-go/hcloud"
)

var HETZNER_API_TOKEN = os.Getenv("HETZNER_API_TOKEN")
var HETZNER_FIREWALL = os.Getenv("HETZNER_FIREWALL")

func GetSSHFirewallRules() []string {
	var sshSources []string
	client := hcloud.NewClient(hcloud.WithToken(HETZNER_API_TOKEN))
	firewall, _, _ := client.Firewall.Get(context.Background(), HETZNER_FIREWALL)
	for _, rule := range firewall.Rules {
		if rule.Direction == hcloud.FirewallRuleDirectionIn {
			if rule.Port != nil && *rule.Port == "22" {
				for _, sourceIP := range rule.SourceIPs {
					sshSources = append(sshSources, sourceIP.String())
				}
			}
		}
	}

	return sshSources
}

// bender slackbot methods
func ListAllServers() []*hcloud.Server {
	client := hcloud.NewClient(hcloud.WithToken(HETZNER_API_TOKEN))
	servers, _ := client.Server.All(context.Background())
	return servers
}

func GetFirewall(id string) *hcloud.Firewall {
	client := hcloud.NewClient(hcloud.WithToken(HETZNER_API_TOKEN))
	firewall, _, _ := client.Firewall.Get(context.Background(), HETZNER_FIREWALL)
	return firewall
}

func ApplyFirewall(id int, label string) string {
	response := ""

	var (
		ctx      = context.Background()
		firewall = &hcloud.Firewall{ID: id}
	)

	resources := []hcloud.FirewallResource{
		{
			Type:          hcloud.FirewallResourceTypeLabelSelector,
			LabelSelector: &hcloud.FirewallResourceLabelSelector{Selector: label},
		},
	}

	client := hcloud.NewClient(hcloud.WithToken(HETZNER_API_TOKEN))
	_, resp, err := client.Firewall.ApplyResources(ctx, firewall, resources)
	if err != nil {
		response = fmt.Sprintf("Error applying firewall: %s\n", err.Error())
	} else {
		response = fmt.Sprintf("Successfully applied firewall: %s\n", resp.Body)
	}

	return response
}

func DeleteServer(serverID int) string {
	result := fmt.Sprintf("Successfully deleted server %d: ", serverID)

	client := hcloud.NewClient(hcloud.WithToken(HETZNER_API_TOKEN))
	server, _, err := client.Server.GetByID(context.Background(), serverID)
	if err != nil {
		return fmt.Sprintf("Server %d doesn't exist!\n", serverID)
	}

	deleteResult, _, err2 := client.Server.DeleteWithResult(context.Background(), server)
	if deleteResult.Action.Status == "error" || err2 != nil {
		return fmt.Sprintf("Unable to delete server [%d] %s: %s\n", serverID, server.Name, err2.Error())
	}

	return result + server.Name
}

func UpdateDNSentry(ipAddr string, ttl string, recordType string,
	name string, recordID string, zoneID string, HETZNER_DNS_API_TOKEN string) string {
	// https://dns.hetzner.com/api-docs/#operation/UpdateRecord

	json := []byte(`{
		"value": "` + ipAddr +
		`","ttl": ` + ttl +
		`,"type": "` + recordType +
		`","name": "` + name +
		`","zone_id": "` + zoneID + `"}`)
	body := bytes.NewBuffer(json)

	// Create client
	client := &http.Client{}

	// Create request
	req, _ := http.NewRequest("PUT", "https://dns.hetzner.com/api/v1/records/"+recordID, body)

	// Headers
	req.Header.Add("Content-Type", "application/json")
	req.Header.Add("Auth-API-Token", HETZNER_DNS_API_TOKEN)

	// Fetch Request
	_, err := client.Do(req)
	if err != nil {
		return "Failure : " + err.Error()
	}
	return ""
}
