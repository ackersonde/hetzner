#!/bin/bash
echo -n "$ROOT_HETZNER_PUB_KEY_B64" | base64 -d | tee /root/.ssh/id_ed25519.pub >/dev/null
chmod 400 /root/.ssh/id_ed25519.pub
echo -n "$ROOT_HETZNER_PUB_CERT_B64" | base64 -d | tee /root/.ssh/id_ed25519-cert.pub >/dev/null
chmod 400 /root/.ssh/id_ed25519-cert.pub
echo -n "$ROOT_HETZNER_PRIV_KEY_B64" | base64 -d | tee /root/.ssh/id_ed25519 >/dev/null
chmod 400 /root/.ssh/id_ed25519

echo -n "$SERVER_CA_KEYS_PUB_B64" | base64 -d | tee /etc/ssh/user_ca_key.pub >/dev/null
sed -i "$ a TrustedUserCAKeys /etc/ssh/user_ca_key.pub" /etc/ssh/sshd_config
# NOTE: due to SSHD_CONFIG changes, we need to restart sshd but can't because we're in a cloud-init script
# this means the above won't go into effect until you manually restart sshd or reboot the server

mkdir -p /root/traefik/logs /root/traefik/certs /root/traefik/keys /mnt/hetzner_disk/vault_data/core
cat <<EOF > /root/traefik/usersFile
$(echo $HTTP_AUTH_USERSFILE_B64 | base64 -d)
EOF

# Setup Syncthing config
mkdir -p /root/syncthing/config /root/syncthing/2086h-4d0t2
echo ".trashed-*" > /root/syncthing/2086h-4d0t2/.stignore
echo "*.part" >> /root/syncthing/2086h-4d0t2/.stignore
chmod 600 /root/syncthing/2086h-4d0t2/.stignore
echo -n "$SYNCTHING_CONFIG_B64" | base64 -d | tee /root/syncthing/config/config.xml >/dev/null
chmod 600 /root/syncthing/config/config.xml
cat <<EOF > /root/syncthing/config/key.pem
$SYNCTHING_KEY
EOF
chmod 600 /root/syncthing/config/key.pem
cat <<EOF > /root/syncthing/config/cert.pem
$SYNCTHING_CERT
EOF
chmod 644 /root/syncthing/config/cert.pem
chown -R 1000:1000 /root/syncthing

apt-get update

# prepare unattended-upgrades settings
apt-get -y install unattended-upgrades
debconf-set-selections <<EOF
iptables-persistent iptables-persistent/autosave_v4 boolean true
iptables-persistent iptables-persistent/autosave_v6 boolean true
unattended-upgrades unattended-upgrades/enable_auto_updates boolean true
EOF
dpkg-reconfigure -f noninteractive unattended-upgrades
cat > /etc/apt/apt.conf.d/50unattended-upgrades << EOF
Unattended-Upgrade::Allowed-Origins {
    "\${distro_id} stable";
    "\${distro_id} \${distro_codename}-security";
    "\${distro_id} \${distro_codename}-updates";
};
Unattended-Upgrade::Remove-Unused-Dependencies "true";
Unattended-Upgrade::Automatic-Reboot "true";
Unattended-Upgrade::Automatic-Reboot-Time "05:00";
EOF

# prepare and start wireguard
apt-get -y install wireguard ca-certificates curl gnupg lsb-release iptables-persistent nmap

cat > /etc/wireguard/wg.conf << EOF
[Interface]
Address = 10.9.0.1/24,fd42:42:42::1/64
ListenPort = {{WG_DO_HOME_PORT}}
PrivateKey = {{WG_DO_PRIVATE_KEY}}

#pixel6
[Peer]
PublicKey = {{WG_HOME_PUBLIC_KEY}}
AllowedIPs = 10.9.0.2/32,fd42:42:42::2/128
PresharedKey = {{WG_DO_HOME_PRESHAREDKEY}}
EOF

/usr/bin/wg-quick up wg
systemctl enable wg-quick@wg.service

# install Golang
VERSION="1.23.5" # go version
ARCH="arm64" # go archicture
curl -O -L "https://golang.org/dl/go${VERSION}.linux-${ARCH}.tar.gz"
# double check sha256 against https://go.dev/dl/?mode=json
tar -xf "go${VERSION}.linux-${ARCH}.tar.gz"
mv -v go /usr/local
echo "export PATH=$PATH:/usr/local/go/bin:$GOPATH/bin" | tee -a /etc/bash.bashrc
ln -s /usr/local/go/bin/go /usr/local/bin/
ln -s /usr/local/go/bin/gofmt /usr/local/bin/
go version

# prepare and start docker-ce
apt-get -y remove docker docker-engine docker.io containerd runc
curl -fsSL https://download.docker.com/linux/debian/gpg | gpg --dearmor -o /usr/share/keyrings/docker-archive-keyring.gpg
echo \
  "deb [arch=$(dpkg --print-architecture) signed-by=/usr/share/keyrings/docker-archive-keyring.gpg] https://download.docker.com/linux/debian \
  $(lsb_release -cs) stable" | tee /etc/apt/sources.list.d/docker.list >/dev/null
apt-get update
apt-get -y install docker-ce docker-ce-cli docker-compose-plugin containerd.io

systemctl start docker
systemctl enable docker

# setup ipv6 capability in docker
mkdir /etc/docker
cat > /etc/docker/daemon.json <<EOF
{
  "ipv6": true,
  "fixed-cidr-v6": "fe00::/80",
  "experimental": true,
  "ip6tables": true
}
EOF
systemctl restart docker
usermod -aG docker ackersond
service ssh restart # so login keys work (which they won't due to new server keys)

touch ~/.hushlogin
